import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput } from 'react-native';
import style from './HeaderStyles';

class Header extends Component {
    state = {
        location: '13th main, Srinivasnagar',
    }

    render() {
        return (
            <View style={style.header}>
                <View style={style.headerRow1}>
                    <View style={style.userImageView}>
                        <Image style={{ width: 55, height: 55 }} source={require('../../Images/avtar.png')} />
                    </View>
                    <View style={style.locationView}>
                        <Text style={{ color: 'white' }} >Your Location</Text>
                        <Text style={{ color: 'white', fontSize: 20, marginTop: 5 }} numberOfLines={1} ellipsizeMode='tail'>{this.state.location}</Text>
                    </View>
                    <View style={style.scanImageView}>
                        <Image style={{ width: 30, height: 30 }} source={require('../../Images/scan.png')} />
                    </View>
                    <View style={style.notifiImageView}>
                        <Image style={{ width: 30, height: 30 }} source={require('../../Images/notification.png')} />
                    </View>
                    <View style={style.menuImageView}>
                        <Image style={{ width: 32, height: 24 }} source={require('../../Images/menuBar.png')} />
                    </View>
                </View>
                <View style={style.headerRow2}>
                    <View style={style.searchIconView}>
                        <Image style={{ width: 30, height: 30 }} source={require('../../Images/search.png')} />
                    </View>
                    <View style={style.searchInputView}>
                        <TextInput
                            style={{ height: 45, fontSize: 20 }}
                            placeholder="Search people & places"
                        />
                    </View>
                    <View style={style.microPhoneIconView}>
                        <Image style={{ width: 22, height: 22 }} source={require('../../Images/microPhone.png')} />
                    </View>
                </View>
            </View>
        );
    }
}

export default Header;