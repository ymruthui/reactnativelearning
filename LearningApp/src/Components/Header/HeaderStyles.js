import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    header: {
        backgroundColor: '#EDA60E',
        flexDirection: 'column',
        padding: 3,
        paddingLeft: 5,
    },
    headerRow1: {
        flexDirection: 'row',
    },
    headerRow2: {
        backgroundColor: 'white',
        marginHorizontal: 2,
        marginBottom: 5,
        flexDirection: 'row'
    },
    userImageView: {
        flex: 1,
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
    },
    locationView: {
        flex: 3,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    scanImageView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    notifiImageView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    menuImageView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    searchIconView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    searchInputView: {
        flex: 8
    },
    microPhoneIconView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default style;